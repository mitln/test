<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function getUserAge($code)
    {
        $dob = $this->findBirthday($code);
        return (date('Y') - date('Y',strtotime($dob)));
    }

    public function actionUserage()
    {
        if(Yii::$app->request->queryParams['code']) {
            $code = Yii::$app->request->queryParams['code'];
            $dob = $this->findBirthday($code);
            $age = (date('Y') - date('Y',strtotime($dob)));
            echo '<b>Method 1 - User age : </b>' . $age . '<br />'; 
            //or
            $age = (date('Y') - date('Y',strtotime($this->findBirthYear($code))));            
            echo '<b>Method 2 - User age : </b>' . $age; 
        } 
    }

    public function findBirthday($code)
    {
        $firstDigit = substr($code, 0, 1);
        $century = $this->getCentury($firstDigit);
        $year = $century . substr($code, 1, 2);
        $month = substr($code, 3, 2);
        $day = substr($code, 5, 2);
        return $year.'-'.$month.'-'.$day;
    }

    public function findBirthYear($code)
    {
        $firstDigit = substr($code, 0, 1);
        $century = $this->getCentury($firstDigit);
        return $century . substr($code, 1, 2);
    }

    public function getCentury($firstDigit)
    {
        switch ($firstDigit) {
            case 1:
            case 2:
                $century = 18;
                break;
            case 3:
            case 4:
                $century = 19;
                break;
            case 5:
            case 6:
                $century = 20;
                break;
            default:
                die('Invalid code.');
                break;
        }
        return $century;
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
