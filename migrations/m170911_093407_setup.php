<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m170911_093407_setup
 */
class m170911_093407_setup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'first_name' => Schema::TYPE_TEXT . ' NOT NULL',
            'last_name' => Schema::TYPE_TEXT . ' NOT NULL',
            'email' => Schema::TYPE_TEXT . ' NOT NULL',
            'personal_code' => Schema::TYPE_BIGINT . ' NOT NULL',
            'phone' => Schema::TYPE_BIGINT . ' NOT NULL',
            'active' => Schema::TYPE_BOOLEAN,
            'dead' => Schema::TYPE_BOOLEAN,
            'lang' => Schema::TYPE_TEXT
        ]);
        
        $this->createTable('loan', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_BIGINT . ' NOT NULL',
            'amount' => 'NUMERIC( 10, 2 ) NOT NULL',
            'interest' => 'NUMERIC( 10, 2 ) NOT NULL',
            'duration' => Schema::TYPE_INTEGER . ' NOT NULL',
            'start_date' => Schema::TYPE_DATE . ' NOT NULL',
            'end_date' => Schema::TYPE_DATE . ' NOT NULL',
            'campaign' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER
        ]);

        $this->saveUserData();
        $this->saveLoanData();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');
        $this->dropTable('loan');
    }

    /**
    * Custom functions to import user & load data
    */
    public function loadJsonFile($file)
    {
        $loadJson = file_get_contents($file);
        return json_decode($loadJson, true);
    }
    
    public function prepareData($file)
    {
        $records = $this->loadJsonFile($file);
        $dataArray = [];
        foreach ($records as $record) {
            isset($record['start_date']) ? $record['start_date'] = date("Y-m-d", $record['start_date'])  : '';
            isset($record['end_date']) ? $record['end_date'] = date("Y-m-d", $record['end_date'])  : '';
            $dataArray[] = $record;
        }
        return $dataArray;
    }
    public function saveUserData()
    {
        $data = $this->prepareData("/vagrant/users.json");
        $this->batchInsert('user', array_keys($data[0]) , array_values($data));
    }
    public function saveLoanData()
    {
        $data = $this->prepareData("/vagrant/loans.json");
        $this->batchInsert('loan', array_keys($data[0]) , array_values($data));
    }
}
